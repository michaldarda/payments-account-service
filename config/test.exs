use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :account_service, AccountService.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :account_service, AccountService.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox,
  username: "ecto",
  password: "ecto",
  database: "payments_test",
  size: 1 # Use a single connection for transactional tests
