defmodule AccountService.AccountActionControllerTest do
  use AccountService.ConnCase

  alias AccountService.AccountAction
  #alias AccountService.User
  alias AccountService.Account

  @valid_attrs %{account_id: 42, description: "some content"}
  @invalid_attrs %{}

  setup do
    # user = %User{
    #   email: "john@example.com",
    #   login: "johnexample",
    #   password: "johnexample",
    #   password_confirmation: "johnexample",
    #   crypted_password: "johnexamplef",
    #   token: "mytoken"} |> Repo.insert!

    account = %Account{
      balance: 10.0,
      user_id: 1} |> Repo.insert!

    %AccountAction{
      account_id: account.id,
      description: "Transfered N money to john@example.com"} |> Repo.insert!

    conn = conn()
            |> put_req_header("accept", "application/json")
            |> put_req_header("authorization", "Bearer mytoken")

    {:ok, conn: conn}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, account_action_path(conn, :index)

    assert json_response(conn, 200)
    #["data"] == [%{"description" => "Transfered N money to john@example.com"}]
  end
end
