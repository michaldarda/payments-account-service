defmodule AccountService.ChangesetErrorView do
  use AccountService.Web, :view

  def render("error.json", %{changeset: changeset}) do
    %{errors: changeset}
  end
end
