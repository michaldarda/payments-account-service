defmodule AccountService.AccountBalanceView do
  use AccountService.Web, :view

  def render("show.json", %{account: account}) do
    %{data: render_one(account, AccountService.AccountBalanceView, "account.json")}
  end

  def render("account.json", %{account_balance: account}) do
    %{balance: account.balance}
  end
end
