defmodule AccountService.AccountActionView do
  use AccountService.Web, :view

  def render("index.json", %{account_actions: account_actions}) do
    %{data: render_many(account_actions, AccountService.AccountActionView, "account_action.json")}
  end

  def render("show.json", %{account_action: account_action}) do
    %{data: render_one(account_action, AccountService.AccountActionView, "account_action.json")}
  end

  def render("account_action.json", %{account_action: account_action}) do
    %{description: account_action.description,
      id: account_action.id,
      created_at: account_action.inserted_at }
  end
end
