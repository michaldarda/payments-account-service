defmodule AccountService.HaCheckController do
  use AccountService.Web, :controller

  def show(conn, _params) do
    conn |> text "OK"
  end
end
