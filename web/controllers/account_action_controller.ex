defmodule AccountService.AccountActionController do
  use AccountService.Web, :controller

  alias AccountService.AccountAction
  alias AccountService.Account

  import Ecto.Query

  plug AccountService.Plug.Authenticate

  def index(conn, _params) do
    user = current_user(conn)

    account_actions = ConCache.get_or_store(:cache, cache_key(user.id), fn() ->
      Repo.all(
      from a in AccountAction,
      join: acc in Account, on: a.account_id == acc.id,
      where: acc.user_id == ^user.id,
      limit: 20)
    end)

    render(conn, "index.json", account_actions: account_actions)
  end

  defp cache_key(user_id) do
    "#{user_id}_account_actions"
  end

  defp current_user(conn) do
    conn.assigns.current_user
  end
end
