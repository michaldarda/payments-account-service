defmodule AccountService.AccountBalanceController do
  use AccountService.Web, :controller

  alias AccountService.AccountAction
  alias AccountService.Account
  alias AccountService.User

  import Ecto.Query

  plug AccountService.Plug.Authenticate

  def show(conn, _params) do
    user = current_user(conn)

    account = ConCache.get_or_store(:cache, cache_key(user.id), fn() ->
      from(a in Account,
      where: a.user_id == ^user.id) |> Repo.one!
    end)

    render(conn, "show.json", account: account)
  end

  defp cache_key(user_id) do
    "#{user_id}_account_balance"
  end

  defp current_user(conn) do
    conn.assigns.current_user
  end
end
