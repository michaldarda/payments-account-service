defmodule AccountService.AccountAction do
  use AccountService.Web, :model

  schema "account_actions" do
    field :account_id, :integer
    field :description, :string

    timestamps
  end

  @required_fields ~w(account_id description)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If `params` are nil, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
