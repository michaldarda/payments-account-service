defmodule AccountService.Router do
  use AccountService.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AccountService do
    pipe_through :api

    get "/ha_check", HaCheckController, :show

    get "/account_balance", AccountBalanceController, :show
    resources "/account_actions", AccountActionController, only: [:index]
  end
end
